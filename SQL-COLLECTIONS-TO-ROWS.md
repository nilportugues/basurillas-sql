#SQL: como sacar los resultados de una tabla tipo colección como una fila:

Tipico caso, ejemplo con algo tan común como un libro de receipts.

## Escenario
Queremos extraer en una receipt que contenga todos los ingredients dadas estas 4 tablas, su name y description: 

```
CREATE TABLE IF NOT EXISTS `ingredients` (
  `id_ingredient` int(11) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_ingredient`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE IF NOT EXISTS `ingredient_amount_receipts` (
  `id_ingredient_amount_receipt` int(11) unsigned NOT NULL,
  `id_ingredient` int(11) unsigned NOT NULL DEFAULT '0',
  `amount` int(11) NOT NULL,
  `units` VARCHAR(11) NOT NULL,
  PRIMARY KEY (`id_ingredient_amount_receipt`),
  KEY `FK_ingredient_amount_receipts_ingredients` (`id_ingredient`),
  CONSTRAINT `FK_ingredient_amount_receipts_ingredients` FOREIGN KEY (`id_ingredient`) REFERENCES `ingredients` (`id_ingredient`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE IF NOT EXISTS `ingredient_receipts` (
  `id_ingredient_receipts` int(11) unsigned NOT NULL,
  `id_receipt` int(11) unsigned NOT NULL DEFAULT '0',
  `id_ingredient` int(11) unsigned NOT NULL DEFAULT '0',
  `orden` int(11) NOT NULL,
  PRIMARY KEY (`id_ingredient_receipts`),
  KEY `FK_ingredient_receipts_ingredients` (`id_ingredient`),
  CONSTRAINT `FK_ingredient_receipts_ingredients` FOREIGN KEY (`id_ingredient`) REFERENCES `ingredients` (`id_ingredient`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `receipts` (
	`id_receipt` INT(10) UNSIGNED NOT NULL,
	`name` VARCHAR(255) NOT NULL COLLATE 'utf8_unicode_ci',
	`description` VARCHAR(255) NOT NULL COLLATE 'utf8_unicode_ci',
	PRIMARY KEY (`id_receipt`)
)
COLLATE='utf8_unicode_ci'
```

## Resolucion

### Paso 1: extraer la amount ingredients para la receipt actual.

```
SELECT COUNT(id_ingredient_receipts) FROM ingredient_receipts WHERE id_receipt = 1;
```

### Paso 2: Montar una left join de "n"

- El "n" sale del count,así que se require un bucle para escribir esta query. 
- Se requeriría de otro bucle para construir los objetos (si fuera necesario).

```
SELECT
	receipts.*,
	ingredient1.name AS ingredient1,
	ingredient_amount_receipts1.amount AS amount1,
	ingredient_amount_receipts1.units AS units1,
	ingredient2.name AS ingredient2,
	ingredient_amount_receipts2.amount AS amount2,
	ingredient_amount_receipts2.units AS units2,
	ingredient3.name AS ingredient3,
	ingredient_amount_receipts3.amount AS amount3
	ingredient_amount_receipts3.units AS units3,

FROM
	ingredient_receipts
##--------------------------------------------------
## LEFT JOIN: receipts
##--------------------------------------------------
LEFT JOIN
        receipts
    ON
        receipts.id_receipt = ingredient_receipts.id_receipt

##--------------------------------------------------
## LEFT JOIN: ingredients1
##--------------------------------------------------
LEFT JOIN
        ingredients AS ingredient1
    ON
        ingredient1.id_ingredient = ingredient_receipts.id_ingredient
        AND ingredient_receipts.orden = 1

##--------------------------------------------------
## LEFT JOIN: amount1
##--------------------------------------------------
LEFT JOIN
        ingredient_amount_receipts AS ingredient_amount_receipts1
    ON
        ingredient_amount_receipts1.id_ingredient = ingredient_receipts.id_ingredient

##--------------------------------------------------
## LEFT JOIN: ingredients2
##--------------------------------------------------
LEFT JOIN
        ingredients AS ingredient2
    ON
        ingredient2.id_ingredient = ingredient_receipts.id_ingredient
        AND ingredient_receipts.orden = 2

##--------------------------------------------------
## LEFT JOIN: amount2
##--------------------------------------------------
LEFT JOIN
        ingredient_amount_receipts AS ingredient_amount_receipts2
    ON
        ingredient_amount_receipts2.id_ingredient = ingredient_receipts.id_ingredient

##--------------------------------------------------
## LEFT JOIN: ingredients3
##--------------------------------------------------
LEFT JOIN
        ingredients AS ingredient3
    ON
        ingredient3.id_ingredient = ingredient_receipts.id_ingredient
        AND ingredient_receipts.orden = 3        

##--------------------------------------------------
## LEFT JOIN: amount3
##--------------------------------------------------
LEFT JOIN
        ingredient_amount_receipts AS ingredient_amount_receipts3
    ON
        ingredient_amount_receipts3.id_ingredient = ingredient_receipts.id_ingredient

WHERE
 	ingredient_receipts.id_receipt = 1

LIMIT 1 	
``` 	

### Paso 3: Montar el objecto

```
<?php
$totalIngredients = 3; //From the very first query! :)

$data = [
	'name'			=> 'Patatas con ajo',
	'description'	=> 'Unas patatas fortísimas'
	'ingredient1' 	=> 'Patatas',
	'amount1' 		=> '1000',
	'units1' 		=> 'gramos',
	'ingredient2' 	=> 'Sal',
	'amount2' 		=> '10',
	'units2' 		=> 'gramos',
	'ingredient3' 	=> 'Ajo',
	'amount3' 		=> '200',
	'units3' 		=> 'gramos',
];

$receipt = (new Receipt())
	->setIdReceipt($data['id_receipt'])
	->setName($data['name'])
	->setDescription($data['description']);

for ($i=1; $i<=$totalIngredients; $i++) {

	$units = new IngredientUnits($data['amount'.$i], $data['units'.$i]);

	$ingredient = new Ingredient($data['ingredient'.$i]);

	$receipt->setIngredient($ingredient, $units));
}

?>
```